# Soul Linux stable 

A secure, stable, fast, lightweight, free and open-source Arch Linux based distribution.

# Our aim
Our aim is to make Linux popular and stable as a Desktop operating system. We deliver enterprise level security to Desktop users. Linux was initially made to be a Desktop OS, but due to it's secure and lightweight nature has become a Server OS. We are hoping to make Linux the best Desktop Operating System. And show the world that Linux can be just as good as a Desktop os. We are not trying to compete with Windows or Mac but we want to get Linux the same popularity as these both operating systems.

# Branches
We have a total of 6 Branches in our main repository. These include the main, development, stable, unsecure, secure-testing and secure Branches. The development branch is we start developmening new feautures and work on updates. Once that's done the code moves to the unsecure branch. From where the vulnerabilities and possible security holes are patched. From here the code moves to the secure-testing brach. In this brach the code is revisited thouroghly for vulnerabilities and other security breaches. The code also goes through a Virtual Machine testing to patch security holes. The kernel is also tested thouroghly. The code then moves to the secure branch where it it cleaned (removing unneeded code). The code in the secure brach is not stable so it goes through some stability tests. From where it goes to the stable brach. The stable branch gives a new update for Soul Linux.

# The development team

 * Venkatesh Mishra (head developer) : experienced in Cybersecurity: vm.opensouce

# Important Note
Soul Linux is currently under development by Venkatesh Mishra, please do not use any of the code until the first release of Soul Linux is publicly released. The development stages are aslo open source to make sure we are following the Security measures to patch the vulnerabilities in our code. Soul Linux is open sourced under the GNU GPL v3 license. Please do wait for the development to be finished. Until then you can help by contributing code. Soul Linux is a community made project and not backed by any company so the development time could take a while. We request you to wait patinelty for the first release.
We hope you enjoy using our Operating System once it's made. Please do co-operate with us and help us by contributing code. We are currently in search of anyone who can help use with the GUI applications for Soul Linux, and are hoping we will soon get people wanting to help us.

- Regards Venkatesh Mishra
